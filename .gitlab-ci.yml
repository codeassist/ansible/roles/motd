---
stages:
    - test


variables:
    GIT_SUBMODULE_STRATEGY: none
    ## System output
    FBGDEF: "\e[0m"
    FBLK: "\e[0;30m"
    FRED: "\e[0;31m"
    FGRN: "\e[0;32m"
    FYEL: "\e[0;33m"
    FBLU: "\e[0;34m"
    FCYA: "\e[0;36m"
    BGBLK: "\e[40m"
    BGRED: "\e[41m"
    BGGRN: "\e[42m"
    BGYEL: "\e[43m"
    BGBLU: "\e[44m"
    BGCYA: "\e[46m"

.tests_set_execution: &tests_set_execution |
    ## Test the role running test playbook(s) in asc order.
    for play in $(ls -v tests/*.yml 2>/dev/null); do
        echo -e ${FBLK}${BGCYA}"Running test${FBGDEF} ${FBLK}${BGCYA}[${FBLK}${BGBLU}${play}${FBLK}${BGCYA}]${FBGDEF} ${FBLK}${BGCYA}for the first time"${FBGDEF}
        ## Run playbook with local connection
        play_log="${CI_JOB_NAME}-$(basename $(mktemp -t XXXXXXXX)).log"
        (docker exec -e TERM=xterm -e ANSIBLE_FORCE_COLOR=1 -e TEST_PLAYBOOK="roles/role_is_being_tested/${play}" ${container} sh -c 'cd /etc/ansible && ansible-playbook ${TEST_PLAYBOOK}' 2>&1 | tee -a ${play_log}) \
            || (echo -e ${FBLK}${BGRED}"================================="${FBGDEF} \
                && echo -e ${FRED}${BGBLK}"Playbook execution error occured!"${FBGDEF} \
                && echo -e ${FBLK}${BGRED}"================================="${FBGDEF} \
                ; printf "\n" \
                ; exit 1)
        ## Run the role/playbook again.
        echo -e ${FBLK}${BGCYA}"Running test${FBGDEF} ${FBLK}${BGCYA}[${FBLK}${BGBLU}${play}${FBLK}${BGCYA}]${FBGDEF} ${FBLK}${BGCYA}again to check idempotence"${FBGDEF}
        idempotence_log="${CI_JOB_NAME}-$(basename $(mktemp -t XXXXXXXX)).log"
        docker exec -e TERM=xterm -e ANSIBLE_FORCE_COLOR=1 -e TEST_PLAYBOOK="roles/role_is_being_tested/${play}" ${container} sh -c 'cd /etc/ansible; ansible-playbook ${TEST_PLAYBOOK}' 2>&1 | tee -a ${idempotence_log}
        ## Checking to make sure it's idempotent.
        grep -q -E 'changed=0.*failed=0' ${idempotence_log} \
            && ( echo -e ${FBLK}${BGGRN}"================================="${FBGDEF} \
                && echo -e ${FGRN}${BGBLK}'Idempotence test - PASSED'${FBGDEF} \
                && echo -e ${FBLK}${BGGRN}"================================="${FBGDEF} \
                ; printf "\n" ) \
            || ( echo -e ${FBLK}${BGRED}"================================="${FBGDEF} \
                && echo -e ${FRED}${BGBLK}'Idempotence test - FAILED!'${FBGDEF} \
                && echo -e ${FBLK}${BGRED}"================================="${FBGDEF} \
                ; printf "\n" \
                ; exit 1 )
    done


.template_test_env: &template_test_env
    stage: test
    image: docker:stable
    services:
        - docker:dind
    only:
        - develop
        - master
    except:
        - tags
    artifacts:
        when: on_failure
        expire_in: 1 day
        paths:
            - ./*.log
    tags:
        - dind

.play_tests: &play_tests
    <<: *template_test_env
    script:
        ## Run the container using the supplied OS.
        - echo -e ${FBLK}${BGCYA}"Starting Docker container${FBGDEF} ${FBL}${BGCYA}[${FBLK}${BGBLU}${container}${FBLK}${BGCYA}]"${FBGDEF}
        - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
        - docker pull gitlab.itsupportme.by:5005/docker/docker-images/ansible/${container}:latest | grep -e 'Pulling from' -e Digest -e Status -e Error
        - docker run --detach --volume="${PWD}":/etc/ansible/roles/role_is_being_tested ${opts} gitlab.itsupportme.by:5005/docker/docker-images/ansible/${container}:latest ${init}

        ## Test running container with a simple, but usefull command
        - echo -e ${FBLK}${BGCYA}"Check if container is running and perform required config"${FBGDEF}
        - docker exec --tty ${container} ansible --version
        ## Copy predefined ansible config
        - (docker exec ${container} cp -f /etc/ansible/roles/role_is_being_tested/tests/ansible.cfg /etc/ansible/) && echo -e ${FBLK}${BGBLU}"[${FBLK}${BGGRN}OK${FBLK}${BGBLU}]"${FBGDEF}
        - printf "\n"

        ## Install dependencies from the dependencies.list if it is present.
        - |
            if test -f ./tests/dependencies.list; then
                while read -r line || [ -n "$line" ]; do
                    echo -e ${FBLK}${BGCYA}"Installing neccessary role from${FBGDEF} ${FBLK}${BGCYA}[${FBLK}${BGBLU}$(eval echo '${line}')${FBLK}${BGCYA}]"${FBGDEF}
                    (docker exec --tty -e TERM=xterm -e PAGER=more -e ROLE_REPO_URL=$(eval echo "${line}") ${container} sh -c 'cd /etc/ansible/roles && git --paginate clone ${ROLE_REPO_URL}') \
                        && echo -e ${FBLK}${BGBLU}"[${FBLK}${BGGRN}OK${FBLK}${BGBLU}]"${FBGDEF}
                done < ./tests/dependencies.list
                printf "\n"
            fi

        ## Run tests for 'local' connection type
        - echo -e ${FBLK}${BGYEL}"---=== *** Run tests with LOCAL connection *** ===---"${FBGDEF}
        ## Copy inventory file with LOCAL connection set
        - docker exec ${container} cp -f /etc/ansible/roles/role_is_being_tested/tests/inventory.local /etc/ansible/hosts
        - *tests_set_execution

        ## Run tests for 'ssh' connection type
        - echo -e ${FBLK}${BGYEL}"---=== *** Run tests with SSH connection *** ===---"${FBGDEF}
        ## Copy inventory file with LOCAL connection set
        - docker exec ${container} cp -f /etc/ansible/roles/role_is_being_tested/tests/inventory.remote /etc/ansible/hosts
        - *tests_set_execution


ubuntu16.04:
    before_script:
        ## Set vars for Docker container launch.
        - container="${CI_JOB_NAME}"
        - init="/lib/systemd/systemd"
        - opts="--name ${container} --security-opt seccomp=unconfined --cap-add=ALL --tmpfs /run --tmpfs /run/lock --volume=/var/lib/docker --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro"
    <<: *play_tests

ubuntu18.04:
    before_script:
        ## Set vars for Docker container launch.
        - container="${CI_JOB_NAME}"
        - init="/lib/systemd/systemd"
        - opts="--name ${container} --security-opt seccomp=unconfined --cap-add=ALL --tmpfs /run --tmpfs /run/lock --volume=/var/lib/docker --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro"
    <<: *play_tests

centos7:
    before_script:
        ## Set vars for Docker container launch.
        - container="${CI_JOB_NAME}"
        - init="/usr/lib/systemd/systemd"
        - opts="--name ${container} --security-opt seccomp=unconfined --cap-add=ALL --tmpfs /run --tmpfs /tmp --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro"
    <<: *play_tests

alpine3.7:
    before_script:
        ## Set vars for Docker container launch.
        - container="${CI_JOB_NAME}"
        - init="/sbin/init"
        - opts="--name ${container} --security-opt seccomp=unconfined --cap-add=ALL --volume=/var/lib/docker --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro"
    <<: *play_tests

alpine3.8:
    before_script:
        ## Set vars for Docker container launch.
        - container="${CI_JOB_NAME}"
        - init="/sbin/init"
        - opts="--name ${container} --security-opt seccomp=unconfined --cap-add=ALL --volume=/var/lib/docker --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro"
    <<: *play_tests
