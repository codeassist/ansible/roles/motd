|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/motd/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/motd/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/motd/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/motd/commits/develop)

## Ansible Role
### **_motd_**

An Ansible role configures welcome screen when user logging into Debian/Ubuntu system.

## Requirements

  - Ansible 2.5 and higher

## Role Default Variables
```yaml
# TODO
```

## Dependencies

none

## License

MIT

## Author Information

ITSupportMe, LLC
